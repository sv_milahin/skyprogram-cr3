from json import JSONDecodeError
import json


class Posts:

    def __init__(self, path):
        """
        При создании экземпляра нужно указать путь к файлу с данными
        """
        self.path = path

    def load_data(self):
        """
        Загружает данные из файла и возвращает обычный list
        """
        try:
            with open(self.path, 'r', encoding='UTF-8') as fl:
                data = json.load(fl)
            return data
        except FileNotFoundError as err:
            return f'Файл не найден: {err}'
        except JSONDecodeError as err:
            return f'Файл не удалось преобразовать: {err}'

    def get_all(self):
        """
        Возвращает список со всеми данными
        """
        posts = self.load_data()
        return posts

    def get_by_pk(self, pk):
        """
        Возвращает один пост по его номеру
        """
        posts = self.load_data()
        post_ = [
                post for post in posts
                if pk == post['pk']
                ]

        return post_

    def get_by_user(self, user_name):
        """
        Возвращает одного юзера по его номеру
        """
        posts = self.load_data()
        return [
                post for post in posts 
                if user_name == post['poster_name']
                ]

    def get_search(self, query):
        """
        Возвращает пост по запросу
        """
        posts = self.load_data()
        return [
                post for post in posts
                if query.lower() in post['content'].lower()
                ]


class Comments:
    def __init__(self, path):
        """
        При создании экземпляра нужно указать путь к файлу с данными
        """
        self.path = path
    def load_data(self):
        """
        Загружает данные из файла и возвращает обычный list
        """
        with open(self.path, 'r', encoding='UTF-8') as fl:
            data = json.load(fl)
        return data

    def get_comments_by_id(self, post_id):
        """
        Возвращает комментарий по номеру
        """
        comments = self.load_data()
        return [
                comment for comment in comments
                if post_id == comment['post_id']
                ]










