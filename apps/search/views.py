from flask import Blueprint, request,  render_template,  redirect, url_for


from config import settings
from utils.file import Posts

bp_search = Blueprint('bp_search', __name__)

posts_all = Posts(f'{settings.APP_DIR}/data/posts.json')

@bp_search.route('/search/')
def search_page():
    s = request.args.get('s')
    search = posts_all.get_search(s)
    if not s or not search:
        return redirect(url_for('bp_posts.index_page'))
    else:
        return render_template('search/search.html', s=s, search=search)
