from flask import Flask

from apps.posts.views import bp_posts
from apps.search.views import bp_search
from apps.users.views import bp_users


from apps.api import api

from config import settings


app = Flask(__name__,
            template_folder=f'{settings.APP_DIR}/templates',
            static_folder=f'{settings.APP_DIR}/static'
            )


app.config.from_object(settings.DevelopmentConfig)
app.register_blueprint(bp_posts)
app.register_blueprint(bp_search)
app.register_blueprint(bp_users)
app.register_blueprint(api, url_prefix='/api')



@app.errorhandler(404)
def error_page(error):
    return f'{error}', 404

@app.errorhandler(500)
def error_page(error):
    return f'{error}', 500
