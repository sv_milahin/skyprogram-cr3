from flask import Blueprint, render_template, abort

from config import settings
from utils.file import Posts, Comments

bp_posts = Blueprint('bp_posts', __name__)

posts_all = Posts(f'{settings.APP_DIR}/data/posts.json')
comments_all = Comments(f'{settings.APP_DIR}/data/comments.json')

@bp_posts.route('/')
def index_page():
   posts = posts_all.get_all()
   return render_template('posts/index.html', posts_all=posts)


@bp_posts.route('/posts/<int:pk>')
def post_pk(pk):
   comments = comments_all.get_comments_by_id(pk)
   try:
      post = posts_all.get_by_pk(pk)
      return render_template('posts/post.html', post=post[0], comments=comments)
   except IndexError:
      abort(404)


