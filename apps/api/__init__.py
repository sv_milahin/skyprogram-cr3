from flask import Blueprint, jsonify

from config import settings
from utils.file import Posts

from loguru import logger

logger.add(f'{settings.APP_DIR}/api.log', format='{time} {level} {message}', level='INFO')

api = Blueprint('api', __name__)

posts_all = Posts(f'{settings.APP_DIR}/data/posts.json')


@api.route('/posts')
def posts_all_page():
    posts = posts_all.get_all()
    logger.info('Запрос /api/posts')
    return jsonify(posts)


@api.route('/posts/<int:pk>')
def post_pk_page(pk):
    post = posts_all.get_by_pk(pk)
    logger.info(f'Запрос /api/posts/{pk}')
    return jsonify(post)