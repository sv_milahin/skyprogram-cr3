from flask import Blueprint, render_template, redirect, url_for, abort

from config import settings
from utils.file import Posts

bp_users = Blueprint('bp_users', __name__)

users_all = Posts(f'{settings.APP_DIR}/data/posts.json')


@bp_users.route('/users/<string:username>')
def user_name_page(username):
    user = users_all.get_by_user(username)
    if not user:
        abort(404)
    else:
        return render_template('users/user-feed.html', user_posts=user)


@bp_users.errorhandler(404)
def error_page(error):
    return f'{error}', 404

@bp_users.errorhandler(500)
def error_page(error):
    return f'{error}', 500