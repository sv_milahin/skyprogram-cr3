import pytest

from apps.main import app


def test_api_posts():
    response = app.test_client().get('/api/posts')
    assert response.status_code == 200
    assert len(response.json) == 8



@pytest.mark.parametrize('pk', [1, 2, 3, 4, 5, 6])
def test_api_post(pk):
    response = app.test_client().get(f'/api/posts/{pk}')
    assert response.status_code == 200
    assert len(response.json) == 1
    assert 'content' in response.json[0]
    assert 'pk' in response.json[0]
