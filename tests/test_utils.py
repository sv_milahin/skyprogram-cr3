import pytest

from config.settings import APP_DIR
from utils.file import Posts, Comments



class TestPosts:
    data_posts = Posts(f'{APP_DIR}/data/posts.json')

    def test_load_data(self):
        assert type(self.data_posts.load_data()) == list

    def test_get_all(self):
        data = self.data_posts.get_all()
        assert type(data) == list
        assert len(data) == 8

    @pytest.mark.parametrize('pk', list(range(1, 9)))
    def test_get_by_pk(self, pk):
        assert type(self.data_posts.get_by_pk(pk)) == list
        assert self.data_posts.get_by_pk(pk)[0]['pk'] == pk

    @pytest.mark.parametrize('username', [user['poster_name'] for user in data_posts.load_data()])
    def test_get_by_user(self, username):
        assert self.data_posts.get_by_user(username)[0]['poster_name'] == username

    def test_get_search(self):
        assert type(self.data_posts.get_search('еда')) == list
        assert self.data_posts.get_search('еда')[0]['pk'] == 1


class TestComments:
    data_comments = Comments(f'{APP_DIR}/data/comments.json')

    def test_load_data(self):
        assert type(self.data_comments.load_data()) == list

    def test_get_by_id(self):
       assert type(self.data_comments.get_comments_by_id(1)) == list



