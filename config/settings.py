from pathlib import Path

from dotenv import load_dotenv, find_dotenv

APP_DIR = Path(__file__).parents[1]


class BaseConfig:
    #SECRET_KEY = load_dotenv(find_dotenv())
    pass


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class TestingConfig(BaseConfig):
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = False
